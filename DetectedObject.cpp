//
// Created by hladk on 25.09.2022.
//

#include "DetectedObject.h"


DetectedObject::DetectedObject(vector<Point> contour, Mat frame){
    DetectedObject::contour = contour;
    convexHull(contour, hull);
    approxPolyDP(hull, polygon, 5, true);
    DetectedObject::boundRect = boundingRect(polygon);
    int rectCenterX = (int) (boundRect.x + (boundRect.width / 2));
    int rectCenterY = (int) (boundRect.y + (boundRect.height / 2));
    center = Point(rectCenterX, rectCenterY);
    height = boundRect.height;
    width = boundRect.width;

    Mat rectMat = Mat(frame, boundRect);
    cvtColor(rectMat, rectMat, COLOR_BGR2GRAY);
    threshold(rectMat, rectMat, 200.0, 255.0, THRESH_BINARY);
    nonZeroElementsCount = countNonZero(rectMat);

    whiteConcentration = ((double) nonZeroElementsCount / (double)(rectMat.cols * rectMat.rows)) * 100;
}

const vector<Point> &DetectedObject::getContour() const {
    return contour;
}

const Rect &DetectedObject::getBoundRect() const {
    return boundRect;
}

const Point &DetectedObject::getCenter() const {
    return center;
}

int DetectedObject::getHeight() const {
    return height;
}

int DetectedObject::getWidth() const {
    return width;
}

void DetectedObject::move(DetectedObject detectedObject) {

    movedDistances.push_back(abs(center.y - detectedObject.center.y));
    contour = detectedObject.contour;
    polygon = detectedObject.polygon;
    boundRect = detectedObject.boundRect;
    center = detectedObject.center;
    height = detectedObject.height;
    width = detectedObject.width;
}

double DetectedObject::getSpeed() {
    if (!movedDistances.empty()) {
        return movedDistances[movedDistances.size()-1] * 7.2;
    }
    return 0.0;
}

double DetectedObject::getAvgSpeed() {
    double avgSpeed = 0;
    if (!movedDistances.empty()) {
        for (double distance : movedDistances) avgSpeed += distance;
        avgSpeed /= (double) movedDistances.size();
        return avgSpeed * 7.2;
    }
    return 0.0;
}

int DetectedObject::getNonZeroElementsCount() const {
    return nonZeroElementsCount;
}

void DetectedObject::setNonZeroElementsCount(int nonZeroElementsCount) {
    DetectedObject::nonZeroElementsCount = nonZeroElementsCount;
}

double DetectedObject::getWhiteConcentration() const {
    return whiteConcentration;
}

void DetectedObject::setWhiteConcentration(double whiteConcentration) {
    DetectedObject::whiteConcentration = whiteConcentration;
}




