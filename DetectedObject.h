//
// Created by hladk on 25.09.2022.
//

#ifndef VEHICLE_DETECTION_DETECTEDOBJECT_H
#define VEHICLE_DETECTION_DETECTEDOBJECT_H

#include <vector>
#include <opencv2/imgproc.hpp>
#include "iostream"

using namespace std;
using namespace cv;

class DetectedObject {
private:
    vector<Point> contour;
    vector<Point> hull;
    vector<Point> polygon;
    Rect boundRect;
    Point center;
    int height;
    int width;
    vector<double> movedDistances;
    int nonZeroElementsCount;
    double whiteConcentration;

public:
    DetectedObject(vector<Point> contour, Mat fgFrame);

    const vector<Point> &getContour() const;

    const Rect &getBoundRect() const;

    const Point &getCenter() const;

    int getHeight() const;

    int getWidth() const;

    void move(DetectedObject detectedObject);

    double getAvgSpeed();

    double getSpeed();

    int getNonZeroElementsCount() const;

    void setNonZeroElementsCount(int nonZeroElementsCount);

    double getWhiteConcentration() const;

    void setWhiteConcentration(double whiteConcentration);
};


#endif //VEHICLE_DETECTION_DETECTEDOBJECT_H
