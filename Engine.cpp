//
// Created by Roman Vozka on 19.09.2022.
//

#include <iostream>
#include <map>
#include "Engine.h"

Engine::Engine() {
    videoCapture.open("C:\\Projects\\gitlab\\mendelu\\vehicle-detection\\resources\\video_pro_analyzu.mp4");
    backgroundSubtractor = createBackgroundSubtractorMOG2();
    backgroundSubtractor->setDetectShadows(false);
//    backgroundSubtractor->setShadowValue(0);
    windowSize = Size(640, 480);
    createDetectionPolygons();
}

void Engine::run() {

    vector<vector<Point>> contoursVector;

    // init frames
    videoCapture.read(prevFrame);
    videoCapture.read(frame);

    unsigned int movingDownCount = 0;
    unsigned int movingUpCount = 0;
    unsigned int whiteCount = 0;
    unsigned int frameIdx = 0;

    while (videoCapture.isOpened() && !isKeyStroked(27)) {

        captureCameraFrame();
        computeForegroundMask();

        findContours(fgFrame, contoursVector, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
        frame.copyTo(processedFrame);

        // process detected contours
        for (auto &contour: contoursVector) {
            double contArea = contourArea(contour);
            // track only objects with area grater than defined minimal area (remove noise)
            if (contArea > MIN_DETECT_CONTOUR_AREA) {

                DetectedObject detectedObject = DetectedObject(contour, frame);

                if (detectedObject.getWidth() < windowSize.width * 0.5 &&
                    detectedObject.getHeight() > windowSize.height * 0.08) {

                    double belongsToLeftPolygon = pointPolygonTest(leftDetectionPolygon, detectedObject.getCenter(), false);
                    double belongsToRightPolygon = pointPolygonTest(rightDetectionPolygon, detectedObject.getCenter(), false);

                    if (belongsToLeftPolygon > 0) {
                        trackVehicle(detectedObject, leftVehicles, movingDownCount, whiteCount, SCALAR_RED);

                    } else if (belongsToRightPolygon > 0) {
                        trackVehicle(detectedObject, rightVehicles, movingUpCount, whiteCount, SCALAR_GREEN);
                    }
                }
            }
        }

        cv::putText(processedFrame, //target image
                    to_string(movingDownCount), //text
                    Point(10, 40), //position
                    FONT_HERSHEY_DUPLEX,
                    1.0,
                    SCALAR_RED, //font color
                    1);

        cv::putText(processedFrame,
                    "AVG Speed:" + to_string((int) getVehiclesAvgSpeed()),
                    Point((int) (windowSize.width * 0.35), 40),
                    FONT_HERSHEY_DUPLEX,
                    1.0,
                    SCALAR_PINK,
                    1);

        cv::putText(processedFrame,
                    "White CNT:" + to_string(whiteCount),
                    Point((int) (windowSize.width * 0.35), 100),
                    FONT_HERSHEY_DUPLEX,
                    1.0,
                    SCALAR_WHITE,
                    1);

        cv::putText(processedFrame,
                    to_string(movingUpCount),
                    Point((int) (windowSize.width * 0.95), 40),
                    FONT_HERSHEY_DUPLEX,1.0,
                    SCALAR_GREEN, //font color
                    1);

        drawContours(processedFrame, detectionPolygons, -1, SCALAR_PINK, 1);

        // show frames
        imshow("Original Frames", frame);
        imshow("Foreground Frame", fgFrame);
        imshow("Processed Frame", processedFrame);

        frameIdx++;
    }
}

void Engine::captureCameraFrame() {
    prevFrame = frame;
    videoCapture >> frame;
    resize(frame, frame, windowSize);
}

bool Engine::isKeyStroked(char key) {
    return waitKey(1) == key;
}

void Engine::computeForegroundMask() {
    backgroundSubtractor->apply(frame, fgFrame);

    erode(fgFrame, fgFrame, STRUCT_ELEMENT_RECT_3x3);
    dilate(fgFrame, fgFrame, STRUCT_ELEMENT_RECT_3x3);
    erode(fgFrame, fgFrame, STRUCT_ELEMENT_RECT_5x5);
    dilate(fgFrame, fgFrame, STRUCT_ELEMENT_RECT_5x5);
}

void Engine::computeForegroundMaskUsingDiff() {
    Mat prevFrameClone = prevFrame.clone();
    Mat actualFrameClone = frame.clone();
    cvtColor(prevFrameClone, prevFrameClone, COLOR_BGR2GRAY);
    cvtColor(actualFrameClone, actualFrameClone, COLOR_BGR2GRAY);
    // get abs difference between actual and previous frame
    absdiff(prevFrameClone, actualFrameClone, fgFrame);
    // get bi-level (binary) image out of grayscale image
    // set threshold value to reduce noise but keep significant objects
    threshold(fgFrame, fgFrame, 45, 255.0, THRESH_BINARY);
    cv::GaussianBlur(fgFrame, fgFrame, cv::Size(1, 5), 0);
    dilate(fgFrame, fgFrame, STRUCT_ELEMENT_RECT_5x7);
    erode(fgFrame, fgFrame, STRUCT_ELEMENT_RECT_3x5);
}

void Engine::createDetectionPolygons() {
    createLeftDetectionPolygon();
    createRightDetectionPolygon();
    detectionPolygons.push_back(leftDetectionPolygon);
    detectionPolygons.push_back(rightDetectionPolygon);
}

void Engine::createLeftDetectionPolygon() {
    // top left point
    leftDetectionPolygon.emplace_back(windowSize.width * 0.25, windowSize.height * 0.35);
    // top right point
    leftDetectionPolygon.emplace_back(windowSize.width * 0.48, windowSize.height * 0.35);
    // bottom right point
    leftDetectionPolygon.emplace_back(windowSize.width * 0.65, windowSize.height);
    // bottom left point
    leftDetectionPolygon.emplace_back(0, windowSize.height);
}

void Engine::createRightDetectionPolygon() {
    // top left point
    rightDetectionPolygon.emplace_back((int) (windowSize.width * 0.5), (int) (windowSize.height * 0.4));
    // top right point
    rightDetectionPolygon.emplace_back(Point((int) (windowSize.width * 0.75), (int) (windowSize.height * 0.4)));
    // bottom right points
    rightDetectionPolygon.emplace_back(Point((int) windowSize.width, (int) (windowSize.height * 0.72)));
    rightDetectionPolygon.emplace_back(Point((int) windowSize.width, (int) windowSize.height));
    // bottom left point
    rightDetectionPolygon.emplace_back(Point((int) (windowSize.width * 0.66), (int) windowSize.height));
}

    double Engine::getDistanceBetweenPoints(const Point &point1, const Point &point2) {
    double distance = sqrt(pow(point1.x - point2.x, 2) + pow(point1.y - point2.y, 2));
    return distance;
}

void Engine::trackVehicle(const DetectedObject& detectedVehicle, vector<DetectedObject> &vehicles, unsigned int &counter, unsigned int &whiteCounter, const Scalar& drawColor) {

    if (vehicles.empty()) {
        vehicles.push_back(detectedVehicle);
        counter++;
        if (detectedVehicle.getWhiteConcentration() > 10) whiteCounter++;
    } else {
        double minDistance = 99999.99;
        unsigned int vehicleMinDistanceIdx = 0;

        for (unsigned int i = 0; i < vehicles.size(); i++) {
            double distance = getDistanceBetweenPoints(vehicles[i].getCenter(), detectedVehicle.getCenter());
            if (distance < minDistance) {
                minDistance = distance;
                vehicleMinDistanceIdx = i;
            }
        }
        if (minDistance < 100.0) {
            vehicles[vehicleMinDistanceIdx].move(detectedVehicle);
        } else {
            vehicles.push_back(detectedVehicle);
            counter++;
            if (detectedVehicle.getWhiteConcentration() > 10) whiteCounter++;
        }
    }
    rectangle(processedFrame, detectedVehicle.getBoundRect(), drawColor, 1);
    circle(processedFrame, detectedVehicle.getCenter(), 3, drawColor, 1);
    putText(processedFrame,
            to_string(detectedVehicle.getWhiteConcentration()),
            Point(detectedVehicle.getCenter().x + 10, detectedVehicle.getCenter().y),
            FONT_HERSHEY_DUPLEX,0.5,
            SCALAR_WHITE,
            1);
}

double Engine::getVehiclesAvgSpeed() {
    double vehiclesAvgSpeed = 0.0;
    if (!leftVehicles.empty())
        for (DetectedObject vehicle : leftVehicles) vehiclesAvgSpeed += vehicle.getAvgSpeed();
    if (!rightVehicles.empty())
        for (DetectedObject vehicle : rightVehicles) vehiclesAvgSpeed += vehicle.getAvgSpeed();
    if (!leftVehicles.empty() || !rightVehicles.empty())
        vehiclesAvgSpeed /= (double) (leftVehicles.size() + rightVehicles.size());
    return vehiclesAvgSpeed;
}