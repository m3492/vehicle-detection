//
// Created by Roman Vozka on 19.09.2022.
//

#ifndef VEHICLE_DETECTION_ENGINE_H
#define VEHICLE_DETECTION_ENGINE_H

#include <stdio.h>
#include <opencv2/videoio.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/imgproc.hpp>
#include "opencv2/highgui.hpp"
#include "DetectedObject.h"


using namespace cv;
using namespace std;

class Engine {
public:
    Engine();
    void run();

private:
    const int MIN_DETECT_CONTOUR_AREA = 800;

    const Scalar SCALAR_BLACK = Scalar(0, 0, 0);
    const Scalar SCALAR_GREEN = Scalar(0, 255, 0);
    const Scalar SCALAR_PINK = Scalar(255, 5, 255);
    const Scalar SCALAR_RED = Scalar(0, 0, 255);
    const Scalar SCALAR_WHITE = Scalar(255, 255, 255);

    const Mat STRUCT_ELEMENT_RECT_1x1 = getStructuringElement(MORPH_RECT, Size(1, 1));
    const Mat STRUCT_ELEMENT_RECT_3x3 = getStructuringElement(MORPH_RECT, Size(3, 3));
    const Mat STRUCT_ELEMENT_RECT_3x5 = getStructuringElement(MORPH_RECT, Size(3, 5));
    const Mat STRUCT_ELEMENT_RECT_5x5 = getStructuringElement(MORPH_RECT, Size(5, 5));
    const Mat STRUCT_ELEMENT_RECT_5x7 = getStructuringElement(MORPH_RECT, Size(5, 7));
    const Mat STRUCT_ELEMENT_RECT_7x7 = getStructuringElement(MORPH_RECT, Size(7, 7));

    VideoCapture videoCapture;
    Size windowSize;

    Mat prevFrame;
    Mat frame;
    Mat fgFrame;
    Mat processedFrame;

    Ptr<BackgroundSubtractorMOG2> backgroundSubtractor;
    // detection
    vector<vector<Point>> detectionPolygons;
    vector<Point> leftDetectionPolygon;
    vector<Point> rightDetectionPolygon;

    vector<DetectedObject> detectedObjects;
    vector<DetectedObject> leftVehicles;
    vector<DetectedObject> rightVehicles;

    void captureCameraFrame();
    bool isKeyStroked(char key);

    void computeForegroundMask();

    void computeForegroundMaskUsingDiff();

    void createDetectionPolygons();
    void createLeftDetectionPolygon();
    void createRightDetectionPolygon();

    double getDistanceBetweenPoints(const Point &point1, const Point &point2);

    void trackVehicle(const DetectedObject &detectedVehicle, vector<DetectedObject> &vehicles, unsigned int &counter,
                      unsigned int &whiteCounter, const Scalar& drawColor);

    double getVehiclesAvgSpeed();
};


#endif //VEHICLE_DETECTION_ENGINE_H
